import React, { Component } from "react";
import user_img from "../images/icons8-user-30.png";
import compass_img from "../images/icons8-compass-east-30.png";
import followers_img from "../images/followers-30 (1).png";
import following_img from "../images/following-30.png";
import repos_img from "../images/repo-30.png";

class PlayerCard extends Component {
  render() {
    const { playerData } = this.props;
    const {
      followers,
      following,
      public_repos,
      login,
      html_url,
      name,
      location,
    } = playerData;

    return (
      <div>
        <img className="avatar-img" src={playerData.avatar_url}></img>
        <h4>Score: {followers + following + public_repos}</h4>
        <h2>
          <a href={html_url}>{login}</a>
        </h2>
        <ul className="card-list">
          <li>
            <img src={user_img}></img>
            <p className="card-text">{name}</p>
          </li>
          {location === null ? null : (
            <li>
              <img src={compass_img}></img>
              <p className="card-text">{location}</p>
            </li>
          )}
          <li>
            <img src={followers_img}></img>
            <p className="card-text">{followers} followers</p>
          </li>
          <li>
            <img src={following_img}></img>
            <p className="card-text">{following} following</p>
          </li>
          <li>
            <img src={repos_img}></img>
            <p className="card-text">{public_repos} repos</p>
          </li>
        </ul>
      </div>
    );
  }
}
export default PlayerCard;
