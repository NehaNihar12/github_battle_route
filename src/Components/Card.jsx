import React, { Component } from "react";
import user from "../images/icons8-user-48.png";
import star from "../images/icons8-star-48.png";
import fork from "../images/icons8-branch-arrow-48.png";
import issue from "../images/icons8-danger-48.png";

class Card extends Component {
  render() {
    let { key, id, data, mode } = this.props;
    return (
      <div className={`card card-${mode}`} key={key}>
        <h4
          style={{
            padding: "10px 10px",
            fontWeight: "bolder",
            fontSize: "24px",
          }}
        >
          # {id + 1}
        </h4>
        <img className="avatar-img" src={data.owner.avatar_url}></img>
        <h2>
          <a
            style={{ color: "darkRed", cursor: "pointer" }}
            href={data.html_url}
          >
            {data.owner.login}
          </a>
        </h2>
        <ul className="card-list">
          <li>
            <div>
              <img src={user}></img>
            </div>
            <div className="card-text">
              <a
                style={{
                  color: "black",
                  fontWeight: "bold",
                  cursor: "pointer",
                }}
                href={data.owner.html_url}
              >
                {data.owner.login}
              </a>
            </div>
          </li>
          <li>
            <div>
              <img src={star}></img>
            </div>
            <div className="card-text">{data.stargazers_count} stars</div>
          </li>
          <li>
            <div>
              <img src={fork}></img>
            </div>
            <div className="card-text">{data.forks} forks</div>
          </li>
          <li>
            <div>
              <img src={issue}></img>
            </div>
            <div className="card-text">{data.open_issues} issues</div>
          </li>
        </ul>
      </div>
    );
  }
}

export default Card;
