import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import torch from "../images/icons8-torch-39.png";

class Navbar extends Component {
  constructor() {
    super();
    this.state = {
      mode: "bg-light",
    };
  }

  onHandleModes = () => {
    const newMode = this.state.mode === "bg-light" ? "bg-dark" : "bg-light";
    this.setState({ mode: newMode }, () => {
      this.props.handleModes(this.state.mode);
    });
  };
  render() {
    return (
      <div className={`main-nav ${this.state.mode}`}>
        <ul>
          <li>
            <NavLink to="/">Popular</NavLink>
          </li>
          <li>
            <NavLink to="/battle">Battle</NavLink>
          </li>
          <li style={{ marginLeft: "auto", marginRight: "39px" }}>
            <img src={torch} onClick={this.onHandleModes}></img>
          </li>
        </ul>
        {this.props.children}
      </div>
    );
  }
}

export default Navbar;
