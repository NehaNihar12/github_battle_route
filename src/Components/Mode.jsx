import React, { Component } from "react";

class Mode extends Component {
  constructor(props) {
    super(props);
    this.state = this.props;
  }
  state = {
    mode: "light",
  };
  render() {
    return <div>{this.props.children}</div>;
  }
}

export default Mode;
