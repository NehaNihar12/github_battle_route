import React, { Component } from "react";
import cross from "../images/icons8-cross-mark-48.png";

class BattleForm extends Component {
  constructor() {
    super();
    this.state = {
      inputText: "",
      submit: false,
      playerData: {},
    };
  }
  onHandleChange = (e) => {
    this.setState({ inputText: e.target.value });
  };

  fetchPlayerData = (e) => {
    e.preventDefault();

    fetch(`https://api.github.com/users/${this.state.inputText}`)
      .then((res) => res.json())
      .then((jsonData) => {
        this.setState(
          {
            playerData: jsonData,
          },
          () => {
            this.props.handleSubmit(this.state.playerData);
          }
        );
        this.setState({ submit: true });
      });
  };

  onHandleCross = () => {
    this.setState({ submit: false, playerData: {}, inputText: "" }, () => {
      this.props.handleCross(this.state.playerData);
    });
  };

  render() {
    if (!this.state.submit) {
      return (
        <form onSubmit={this.fetchPlayerData}>
          <input
            type="text"
            placeholder="github username"
            value={this.state.inputText}
            onChange={this.onHandleChange}
          ></input>
          <button data-button="submit">SUBMIT</button>
        </form>
      );
    } else {
      return (
        <div className="player-info" style={{ marginRight: "2rem" }}>
          <div>
            <img
              style={{
                height: "81px",
                width: "81px",
                background: "white",
                margin: "0.4rem",
                borderRadius: "50%",
              }}
              src={this.state.playerData.avatar_url}
              alt="player_avatar"
            ></img>
          </div>
          <div style={{ marginLeft: "1rem" }}>
            <a href={this.state.playerData.html_url}>{this.state.inputText}</a>
          </div>

          <button data-button="cross">
            <img src={cross} onClick={this.onHandleCross} alt="cross-btn"></img>
          </button>
        </div>
      );
    }
  }
}

export default BattleForm;
