import React, { Component } from "react";
import { Link } from "react-router-dom";
import PlayerCard from "../Components/PlayerCard";

class PlayerResult extends Component {
  constructor() {
    super();
    const queryParams = new URLSearchParams(window.location.search);

    this.state = {
      player1: {},
      player2: {},
      p1: queryParams.get("playerOne"),
      p2: queryParams.get("playerTwo"),
      score1: 0,
      score2: 0,
    };
  }

  componentDidMount() {
    fetch(`https://api.github.com/users/${this.state.p1}`)
      .then((res) => res.json())
      .then((jsonData) => {
        this.setState(
          {
            player1: jsonData,
          },
          () => {
            const { followers, following, public_repos } = this.state.player1;
            this.setState({ score1: followers + following + public_repos });
          }
        );
      });
    fetch(`https://api.github.com/users/${this.state.p2}`)
      .then((res) => res.json())
      .then((jsonData) => {
        this.setState(
          {
            player2: jsonData,
          },
          () => {
            const { followers, following, public_repos } = this.state.player2;
            this.setState({ score2: followers + following + public_repos });
          }
        );
      });
  }

  render() {
    const { mode } = this.props;
    return (
      <div>
        <div style={{ display: "flex", flexFlow: "row wrap" }}>
          <div className={`card card-${mode}`}>
            {this.state.score1 === this.state.score2 ? (
              <h4>Tie</h4>
            ) : this.state.score1 > this.state.score2 ? (
              <h4>Winner</h4>
            ) : (
              <h4>Loser</h4>
            )}
            <PlayerCard playerData={this.state.player1}></PlayerCard>
          </div>
          <div className={`card card-${mode}`}>
            {this.state.score1 === this.state.score2 ? (
              <h4>Tie</h4>
            ) : this.state.score2 > this.state.score1 ? (
              <h4>Winner</h4>
            ) : (
              <h4>Loser</h4>
            )}
            <PlayerCard playerData={this.state.player2}></PlayerCard>
          </div>
        </div>
        <Link to="/battle">
          <button data-button="submit">Reset</button>
        </Link>
      </div>
    );
  }
}

export default PlayerResult;
