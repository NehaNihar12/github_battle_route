import React, { Component } from "react";
import { Link } from "react-router-dom";

import BattleForm from "../Components/BattleForm";
import user from "../images/icons8-user-100.png";
import battlePlane from "../images/icons8-fighter-jet-100.png";
import trophy from "../images/icons8-trophy-100.png";

class Player extends Component {
  constructor() {
    super();
    this.state = {
      player1: {},
      player2: {},
      battlePath: "",
    };
  }

  handleSubmit1 = (playerData) => {
    this.setState({ player1: playerData }, () => {
      if (Object.keys(this.state.player2).length !== 0) {
        this.setState({
          battlePath: `battle/results?playerOne=${this.state.player1.login}&playerTwo=${this.state.player2.login}`,
        });
      }
    });
  };
  handleSubmit2 = (playerData) => {
    this.setState({ player2: playerData }, () => {
      if (Object.keys(this.state.player1).length !== 0) {
        this.setState({
          battlePath: `results?playerOne=${this.state.player1.login}&playerTwo=${this.state.player2.login}`,
        });
      }
    });
  };

  handleCross1 = () => {
    this.setState({ player1: {} });
  };

  handleCross2 = () => {
    this.setState({ player2: {} });
  };
  handleClick = () => {
    console.log("rendered");
  };

  render() {
    return (
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
          padding: "auto",
        }}
      >
        <h1>Instructions</h1>

        <ul
          data-card="player-card"
          style={{
            listStyle: "none",
            display: "flex",
            alignItems: "center",
            flexDirection: "column",
            flexFlow: "row wrap",
            paddingLeft: "7rem",
            paddingTop: "3rem",
            paddingBottom: "3rem",
          }}
        >
          <li>
            <div data-card="player-card-div">
              <div style={{ selfAlign: "center", paddingLeft: "45px" }}>
                <h3>Enter two Github users</h3>
              </div>
              <div data-card-img="battle-img">
                <img src={user}></img>
              </div>
            </div>
          </li>
          <li>
            <div data-card="player-card-div">
              <div>
                <h3>Battle</h3>
              </div>
              <div data-card-img="battle-img">
                <img src={battlePlane}></img>
              </div>
            </div>
          </li>
          <li>
            <div data-card="player-card-div">
              <div>
                <h3>See the winner</h3>
              </div>
              <div data-card-img="battle-img">
                <img src={trophy}></img>
              </div>
            </div>
          </li>
        </ul>
        <h1>Players</h1>
        <ul
          style={{
            display: "flex",
            listStyle: "none",
            justifyContent: "space-between",
            marginTop: "4rem",
            marginBottom: "4rem",
          }}
        >
          <li>
            <BattleForm
              handleSubmit={this.handleSubmit1}
              handleCross={this.handleCross1}
            ></BattleForm>
          </li>
          <li>
            <BattleForm
              handleSubmit={this.handleSubmit2}
              handleCross={this.handleCross2}
            ></BattleForm>
          </li>
        </ul>

        {Object.keys(this.state.player1).length !== 0 &&
        Object.keys(this.state.player2).length !== 0 ? (
          <Link to={`/battle/${this.state.battlePath}`}>
            <button data-button="submit">Battle</button>
          </Link>
        ) : null}
      </div>
    );
  }
}

export default Player;
