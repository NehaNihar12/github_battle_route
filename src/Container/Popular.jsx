import React, { Component } from "react";

import Card from "../Components/Card";

class Popular extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      Loading: false,
    };
  }

  componentDidMount() {
    this.setState({ Loading: true });
    fetch(
      "https://api.github.com/search/repositories?q=stars:%3E1+language:All&sort=stars&order=desc&type=Repositories"
    )
      .then((res) => res.json())
      .then((jsonData) => {
        this.setState({
          data: jsonData.items,
          Loading: false,
        });
      });
  }

  fetchData = (activeFilter) => {
    this.setState({ Loading: true });
    fetch(
      `https://api.github.com/search/repositories?q=stars:%3E1+language:${activeFilter}&sort=stars&order=desc&type=Repositories`
    )
      .then((res) => res.json())
      .then((jsonData) => {
        this.setState({
          data: jsonData.items,
          Loading: false,
        });
      })
      .catch((err) => console.error("error in data handling"));
  };

  handleFilter = (activeFilter) => {
    this.setState({ filter: activeFilter }, () => {
      this.fetchData(this.state.filter);
    });
  };

  render() {
    const { mode } = this.props;
    if (this.state.Loading)
      return (
        <div>
          <h1> Please wait some time.... </h1>
        </div>
      );
    return (
      <div className="header2">
        <ul>
          <li onClick={() => this.handleFilter("All")}>All</li>
          <li onClick={() => this.handleFilter("JavaScript")}>JavaScript</li>
          <li onClick={() => this.handleFilter("Ruby")}>Ruby</li>
          <li onClick={() => this.handleFilter("Java")}>Java</li>
          <li onClick={() => this.handleFilter("CSS")}>CSS</li>
          <li onClick={() => this.handleFilter("Python")}>Python</li>
        </ul>
        <div style={{ display: "flex", flexFlow: "row wrap" }}>
          {this.state.data.map((data, index) => (
            <Card mode={mode} key={data.id} id={index} data={data}></Card>
          ))}
        </div>
      </div>
    );
  }
}

export default Popular;
