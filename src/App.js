import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";

import "./App.css";
import Mode from "./Components/Mode";
import Navbar from "./Components/Navbar";
import Popular from "./Container/Popular";
import Battle from "./Container/Battle";
import PlayerResult from "./Container/PlayerResult";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      dataLoaded: false,
      mode: "bg-light",
    };
  }

  setMode = () => {
    const newMode = this.state.mode === "bg-light" ? "bg-dark" : "bg-light";
    this.setState({ mode: newMode });
  };
  render() {
    return (
      <div className={`main ${this.state.mode}`}>
        <Mode>
          <Navbar handleModes={this.setMode}></Navbar>
          <Switch>
            <Route exact path="/">
              <Popular mode={this.state.mode} />
            </Route>
            <Route exact path="/battle">
              <Battle mode={this.state.mode} />
            </Route>
            <Route path="/battle/*">
              <PlayerResult mode={this.state.mode} />
            </Route>
          </Switch>
        </Mode>
      </div>
    );
  }
}
export default App;
